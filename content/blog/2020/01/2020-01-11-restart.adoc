= Restart 2020
xpj
2020-01-15
:jbake-type: post
:jbake-status: published
:jbake-tags: bigclown, bike, projects
:idprefix:

After almost two years of silence, I'm returning with new projects.

During the last two years, I migrated from Wemos and other Arduino-like microcontrollers to HARDWARIO IoT (formerly BigClown).
I backed on IndieGoGo BigClown's campaign https://www.indiegogo.com/projects/bigclown-the-iot-kit-for-makers-from-makers#/[BigClown: The IoT Kit for Makers. From Makers.]
I received big suitcase full of different modules (like flooding sensor, power controller or LCD thermostat) and now I'm playing
with modules and planning new projects.

Currently, the only finished project with BigClown is https://bitbucket.org/xpjninja/bigclown-rain-sensor[rain sensor]
utilizing tipping bucket rain gauge from broken weather station.

My ongoing projects at this moment are following:

* Water Tank Sensor
* Security Sensor
* Extend Rain sensor with other parts to make full weather station (wind speed and direction sensors)
* JetBrains IntelliJ IDEA plugin for JKS (Java KeyStore)

My future projects:

* Indoor Weather Station based on BigClown and e-paper displaying data from external weather sensors
* Custom Classic Bike Build on a steel frame with Shimano RX100 components
* Programmable IR remote for https://geosmart.eu/moon-lander-0[GeoSmart Moon Lander]

I will describe details of my projects in following posts.